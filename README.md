# porous_block_example

A boundary layer flow encounters a porous element, which deflects the flow at a pi/4 angle.

A mesh is included.  To Run:

topoSet

porousSimpleFoam


![Alt text](streamlines_block.png )

![Alt text](streamline_block_topview.png )